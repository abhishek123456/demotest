package com.Mpower.Testbase;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.log4testng.Logger;

public class ReusableFunction {
	
	
		//WebDriver driver;
		public static WebDriver driver;
		//code for Write Logger 
	//	public static Logger logger = LoggerFactory.getLogger(DashboardMpowerTest.class);
		
		//wrting report for brachview module...
	//	public static Logger BranchViewLogger= LoggerFactory.getLogger(Branch_ViewTest.class);
		
		//Function for Print the steps in allure report
//			@Step("{0}")
			 public static void logStep(String stepName){
			
			 }
			
			//Function for take the screen shot in allure report
//			@Attachment("Screenshot")
		    public static byte[] attachScreen(WebDriver driver ) {
		        logStep("Taking screenshot");
		        return ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
		    }
			
//			@Attachment("Error_Screenshot")
		    public static byte[] attachScreen_TestCaseError(WebDriver driver) {
		        logStep("Taking screenshot");
		        return ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES); 
		    }
			
		
		// Function for Read the data from Properties File
		public static File file = null;
		public static FileInputStream fileInput = null;
	    //public String filepath="D:\\Max_Life Resolved Issues\\MAX_QA_FunctionalTest\\File Paths\\geckodriver.exe";
		public static Properties prop;
		
		public static Properties readProperties()
		{
			file = new File(System.getProperty("user.dir") + "\\src\\test\\resources\\Input.properties");
			FileInputStream fileInput = null;

			try {
				fileInput = new FileInputStream(file);
				prop = new Properties();
				prop.load(fileInput);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

			catch (IOException e1) {
//				logger.error(e1.getMessage());
			}
		
		return prop;
		}
		
		
		public static WebElement waitTillElementLocated(WebElement element) {
			WebDriverWait wait = new WebDriverWait(driver, 20);
			WebElement elementloaded = wait.until(ExpectedConditions.elementToBeClickable(element));
			return elementloaded;
		}
	    
		public static WebElement waitTillElementVisible(WebElement element) {
			WebDriverWait wait = new WebDriverWait(driver, 20);
			WebElement elementloaded = wait.until(ExpectedConditions.visibilityOf(element));
			return elementloaded;
		}
		
		public static WebElement waitTillElementToBeClickable(WebElement element) {
			WebDriverWait wait = new WebDriverWait(driver, 20);
			WebElement elementloaded = wait.until(ExpectedConditions.elementToBeClickable(element));
			return elementloaded;
		}
		
		public static WebElement waitTillpresenceofElementLoacted(By element) {
			WebDriverWait wait = new WebDriverWait(driver, 20);
			WebElement elementloaded = wait.until(ExpectedConditions.presenceOfElementLocated(element));
			return elementloaded;
		}
		
		
		public static void actionsClick(WebElement element) {

			Actions action = new Actions(driver);
			action.moveToElement(element).click().perform();

		}

		public static void click(WebElement locator) throws Exception {
			Thread.sleep(1000);

			for (int i = 0; i <= 40; i++) {
				try {
					locator.click();
					break;

				} catch (Exception e) {
					if (i == 40) {
						throw e;

					} else {
						Thread.sleep(1000);
					}
				}
			}
		}

		public static void waitForVisible(WebElement locator) throws Exception {
			Thread.sleep(1000);
			for (int i = 0; i <= 40; i++) {
				try {
					locator.isDisplayed();
					break;

				} catch (Exception e) {
					if (i == 40) {
						throw e;

					} else {
						Thread.sleep(1000);
					}
				}
			}
		}
//Scroll window down
		public static void jsClick() throws Exception {
			try {
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("window.scrollBy(0,250)", "");

			} catch (Exception e) {
				e.getMessage();
				throw e;
			}

		}
//Scroll window up
		public static void jsClickup() throws Exception {
			try {
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("window.scrollBy(0,-250)", "");

			} catch (Exception e) {
				e.getMessage();
				throw e;
			}

		}

		public static void clickByJS1(By by) {
			try {
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", by);

			} catch (Exception e) {
				e.getMessage();
			}
		}
		// WebElement element =
		// driver.findElement(By.xpath("//div[@id='renewalList']/div/button"));
		public static void clickByJS(WebElement el) {
			try {
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", el);

			} catch (Exception e) {
				e.getMessage();
			}
		}
		
		public static void waitForElement(WebDriver driver, By by, long timeDuration) throws Exception {
			try {
				// ---TimeDuration will be in
				// seconds--------------------------------------
				(new WebDriverWait(driver, timeDuration)).until(ExpectedConditions.visibilityOfElementLocated(by));
				Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
		}
		public static String readingdata(int sheetno, int rownum, int colnum) throws Exception {
			File file = new File(System.getProperty("user.dir") + "\\src\\test\\resources\\Maxlife_Testdata.xlsx");
			FileInputStream fileInputStream = new FileInputStream(file);
			XSSFWorkbook hssfWorkbook = new XSSFWorkbook(fileInputStream);
			XSSFSheet sheet = hssfWorkbook.getSheetAt(sheetno);
			XSSFCell cell = sheet.getRow(rownum).getCell(colnum);
			DataFormatter df = new DataFormatter();
			String data = df.formatCellValue(cell);
			hssfWorkbook.close();
			return data;

		}

		public static void type(WebElement textbox, String inputdata) throws Exception {
			Thread.sleep(1000);
			for (int i = 0; i <= 15; i++) {
				try {
					textbox.clear();
					textbox.sendKeys(inputdata);
					break;

				} catch (Exception e) {
					if (i == 15) {
						throw e;

					} else {
						Thread.sleep(1000);
					}
				}
			}
		}

		public static void scrollElementToViewPoint(WebElement element) throws InterruptedException {
			Actions actions = new Actions(driver);
			actions.moveToElement(element).build().perform();
			Thread.sleep(5000);
			// waitTillPageLoaded(driver);
			// Thread.sleep(3000);
		}

		public static void scrollDiv(By by) {
			try {
				JavascriptExecutor je = (JavascriptExecutor) driver;
				WebElement teagueTool = driver.findElement(by);
				je.executeScript("arguments[0].scrollIntoView(true);", teagueTool);
				Thread.sleep(50);
			} catch (Exception e) {
				System.out.println("Could not scrolled this Element :" + driver.findElement(by));
				e.printStackTrace();
				e.getMessage();
			}
		}

		public static void waitTillPageLoaded(WebDriver driver) {
			ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
				public Boolean apply(WebDriver driver) {
					return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
				}
			};
			Wait<WebDriver> wait = new WebDriverWait(driver, 30);
			try {
				wait.until(expectation);
			} catch (Throwable error) {
			}
		}

		public static void captureScreenShot(WebDriver ldriver) {
			// Take screenshot and store as a file format
			File src = ((TakesScreenshot) ldriver).getScreenshotAs(OutputType.FILE);
			try {
				// now copy the screenshot to desired location using copyFile method

				FileUtils.copyFile(src, new File("C:/selenium/" + System.currentTimeMillis() + ".png"));
			} catch (IOException e)

			{
				System.out.println(e.getMessage());
			}

		}
		//function for implicitly wait for webelement... 
		public static  void ImplicitalyWaitElement(int duration) {
			driver.manage().timeouts().implicitlyWait(duration, TimeUnit.SECONDS);
		}
		//function to select element  from DropDown through value
		public static void SelectByValue_fromDropDown(WebElement element,String value) {
			
			Select select =new Select(element);
			select.selectByValue(value);
				
		}
		//function to select element from from dropdown through index value
	    public static void SelectByIndex_fromDropDown(WebElement element,int index) {
			
			Select select =new Select(element);
			select.selectByIndex(index);
				
			
		}
	    //function to select element from from dropdown through visible text
	    public static void SelectByVisibleText_fromDropDown(WebElement element,String text) {
			
			Select select =new Select(element);
			select.selectByVisibleText(text);
				
			
		}
		
	

	
	

}
